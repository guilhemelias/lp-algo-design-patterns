package examples.rpg;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.*;

public class FactoryTest {

    private ByteArrayOutputStream output;
    private final PrintStream systemOut = System.out;

    @BeforeEach
    private void spySystemOut() {
        output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
    }

    @AfterEach
    private void restoreSystemOut() {
        System.setOut(systemOut);
    }

    @Test
    public void testElfCharacterFactory() {
        runExample(new ElfCharacterFactory());
        assertEquals(
                """
                        Elf Ninja is hiding!
                        Elf Warrior is charging!
                        Elf Wizard is shooting a light ball!""",
                output.toString().trim()
        );
    }

    @Test
    public void testGoblinCharacterFactory() {
        runExample(new GoblinCharacterFactory());
        assertEquals(
                """
                        Goblin Ninja is hiding!
                        Goblin Warrior is charging!
                        Goblin Wizard is shooting an ice beam!""",
                output.toString().trim()
        );
    }

    @Test
    public void testHumanCharacterFactory() {
        runExample(new HumanCharacterFactory());
        assertEquals(
                """
                        Human Ninja is hiding!
                        Human Warrior is charging!
                        Human Wizard is summoning a tornado!""",
                output.toString().trim()
        );
    }

    void runExample(CharacterFactory factory) {
        Ninja ninja = factory.createNinja();
        Warrior warrior = factory.createWarrior();
        Wizard wizard = factory.createWizard();

        ninja.hide();
        warrior.charge();
        wizard.useMagic();
    }
}
