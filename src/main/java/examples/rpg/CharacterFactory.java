package examples.rpg;

public interface CharacterFactory {
    public Ninja createNinja();
    public Warrior createWarrior();
    public Wizard createWizard();
}
