package examples.rpg;

public class GoblinWarrior implements Warrior {

    @Override
    public void charge() {
        System.out.println("Goblin Warrior is charging!");
    }
}
