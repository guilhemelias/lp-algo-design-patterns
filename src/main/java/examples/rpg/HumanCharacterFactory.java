package examples.rpg;

public class HumanCharacterFactory implements CharacterFactory {
    @Override
    public Ninja createNinja() {
        return new HumanNinja();
    }

    @Override
    public Warrior createWarrior() {
        return new HumanWarrior();
    }

    @Override
    public Wizard createWizard() {
        return new HumanWizard();
    }
}
