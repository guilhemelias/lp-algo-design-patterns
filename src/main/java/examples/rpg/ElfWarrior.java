package examples.rpg;

public class ElfWarrior implements Warrior {

    @Override
    public void charge() {
        System.out.println("Elf Warrior is charging!");
    }
}
