package examples.rpg;

public class HumanNinja implements Ninja {
    @Override
    public void hide() {
        System.out.println("Human Ninja is hiding!");
    }
}
