package examples.rpg;

public class ElfCharacterFactory implements CharacterFactory {
    @Override
    public Ninja createNinja() {
        return new ElfNinja();
    }

    @Override
    public Warrior createWarrior() {
        return new ElfWarrior();
    }

    @Override
    public Wizard createWizard() {
        return new ElfWizard();
    }
}
