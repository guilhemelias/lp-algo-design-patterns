package examples.rpg;

public class GoblinNinja implements Ninja {
    @Override
    public void hide() {
        System.out.println("Goblin Ninja is hiding!");
    }
}
