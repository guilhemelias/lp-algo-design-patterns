package examples.rpg;

public class HumanWarrior implements Warrior {

    @Override
    public void charge() {
        System.out.println("Human Warrior is charging!");
    }
}
